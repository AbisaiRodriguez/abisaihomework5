﻿using System;
using System.Diagnostics;
using Prism.Mvvm;
using Prism.Navigation;

namespace Homework5.ViewModels
{
    public class TabContainerViewModel : BindableBase
    {
        public TabContainerViewModel()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(TabContainerViewModel)}:  ctor");
        }
    }
}
